hi clear
syntax reset
let g:colors_name = "otto"
set background=dark
set t_Co=256
hi Normal guifg=#f0dada ctermbg=NONE guibg=#2c3746 gui=NONE

hi DiffText guifg=#ff716a guibg=NONE
hi ErrorMsg guifg=#ff716a guibg=NONE
hi WarningMsg guifg=#ff716a guibg=NONE
hi PreProc guifg=#ff716a guibg=NONE
hi Exception guifg=#ff716a guibg=NONE
hi Error guifg=#ff716a guibg=NONE
hi DiffDelete guifg=#ff716a guibg=NONE
hi GitGutterDelete guifg=#ff716a guibg=NONE
hi GitGutterChangeDelete guifg=#ff716a guibg=NONE
hi cssIdentifier guifg=#ff716a guibg=NONE
hi cssImportant guifg=#ff716a guibg=NONE
hi Type guifg=#ff716a guibg=NONE
hi Identifier guifg=#ff716a guibg=NONE
hi PMenuSel guifg=#2aacaa guibg=NONE
hi Constant guifg=#2aacaa guibg=NONE
hi Repeat guifg=#2aacaa guibg=NONE
hi DiffAdd guifg=#2aacaa guibg=NONE
hi GitGutterAdd guifg=#2aacaa guibg=NONE
hi cssIncludeKeyword guifg=#2aacaa guibg=NONE
hi Keyword guifg=#2aacaa guibg=NONE
hi IncSearch guifg=#ffc46b guibg=NONE
hi Title guifg=#ffc46b guibg=NONE
hi PreCondit guifg=#ffc46b guibg=NONE
hi Debug guifg=#ffc46b guibg=NONE
hi SpecialChar guifg=#ffc46b guibg=NONE
hi Conditional guifg=#ffc46b guibg=NONE
hi Todo guifg=#ffc46b guibg=NONE
hi Special guifg=#ffc46b guibg=NONE
hi Label guifg=#ffc46b guibg=NONE
hi Delimiter guifg=#ffc46b guibg=NONE
hi Number guifg=#ffc46b guibg=NONE
hi CursorLineNR guifg=#ffc46b guibg=NONE
hi Define guifg=#ffc46b guibg=NONE
hi MoreMsg guifg=#ffc46b guibg=NONE
hi Tag guifg=#ffc46b guibg=NONE
hi String guifg=#ffc46b guibg=NONE
hi MatchParen guifg=#ffc46b guibg=NONE
hi Macro guifg=#ffc46b guibg=NONE
hi DiffChange guifg=#ffc46b guibg=NONE
hi GitGutterChange guifg=#ffc46b guibg=NONE
hi cssColor guifg=#ffc46b guibg=NONE
hi Function guifg=#fd3762 guibg=NONE
hi Directory guifg=#cb75f7 guibg=NONE
hi markdownLinkText guifg=#cb75f7 guibg=NONE
hi javaScriptBoolean guifg=#cb75f7 guibg=NONE
hi Include guifg=#cb75f7 guibg=NONE
hi Storage guifg=#cb75f7 guibg=NONE
hi cssClassName guifg=#cb75f7 guibg=NONE
hi cssClassNameDot guifg=#cb75f7 guibg=NONE
hi Statement guifg=#5cc6d1 guibg=NONE
hi Operator guifg=#5cc6d1 guibg=NONE
hi cssAttr guifg=#5cc6d1 guibg=NONE


hi Pmenu guifg=#f0dada guibg=#454545
hi SignColumn guibg=#2c3746
hi Title guifg=#f0dada
hi LineNr guifg=#b54949 guibg=#2c3746
hi NonText guifg=#9cacad guibg=#2c3746
hi Comment guifg=#9cacad gui=italic
hi SpecialComment guifg=#9cacad gui=italic guibg=NONE
hi CursorLine guibg=#454545
hi TabLineFill gui=NONE guibg=#454545
hi TabLine guifg=#b54949 guibg=#454545 gui=NONE
hi StatusLine gui=bold guibg=#454545 guifg=#f0dada
hi StatusLineNC gui=NONE guibg=#2c3746 guifg=#f0dada
hi Search guibg=#9cacad guifg=#f0dada
hi VertSplit gui=NONE guifg=#454545 guibg=NONE
hi Visual gui=NONE guibg=#454545
